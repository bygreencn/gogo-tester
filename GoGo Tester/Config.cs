﻿namespace GoGo_Tester
{
    static class Config
    {
        public static int ConnTimeout = 1200;
        public static int MaxThreads = 4;
        public static int PassCount = 2;
        public static long Version = 2014121402;
    }
}
